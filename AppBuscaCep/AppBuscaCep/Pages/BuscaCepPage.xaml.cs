﻿using AppBuscaCep.ViewModels;
using Xamarin.Forms;

namespace AppBuscaCep.Pages
{
    public partial class BuscaCepPage : ContentPage
    {
        public BuscaCepPage()
        {
            InitializeComponent();

            BindingContext = new BuscaCepViewModel();
        }
    }
}